#!/usr/bin/awk -f
#Lecture des logs du firewall pour retourner la liste des machines attaquant
#sur le port 135 ou 6667
#
#Arguments : 
#<fichier(s) � scanner> <autre fichier donc le nom contient blacklist>
# Le second fichier contient les IP des machines suppl�mentaires � sortir.
#
# Format de sortie : Mois jour hh:mm:ss hostname nb_attques
#
# 02/2003 Fr�d�ric Pauget

{ if (FILENAME~"blacklist") {
    if ($0=="") nextfile;
    tentatives[$0]=0;
    dern_tentative[$0]="Vieux 00 00:00:00"; }
}

/.*Virus:IN=eth0.*/{
    gsub("SRC=","",$9);
    tentatives[$9]++;
    dern_tentative[$9]=$1" "$2" "$3;
}

END{
  for (machine in tentatives){
    system("echo "dern_tentative[machine]" $(host "machine" 2>/dev/null | awk '/^Name/ {print $2}') "tentatives[machine])
  }
}
