#!/bin/bash
# Replacer les clefs ZSK de toutes les zones

cd /usr/scripts/var/dnssec

for zone in  0.0.0.0.0.0.0.0.d.3.e.f.0.4.2.0.1.0.a.2.ip6.arpa 140.231.138.in-addr.arpa 147.231.138.in-addr.arpa 243.42.10.in-addr.arpa adm.v6.crans.org g.crans.org 0.0.0.0.d.3.e.f.0.4.2.0.1.0.a.2.ip6.arpa 141.231.138.in-addr.arpa 148.231.138.in-addr.arpa 4.0.0.0.d.3.e.f.0.4.2.0.1.0.a.2.ip6.arpa clubs.ens-cachan.fr tv.crans.org 136.231.10.in-addr.arpa 142.231.138.in-addr.arpa 149.231.138.in-addr.arpa 4.0.8.c.d.3.e.f.0.4.2.0.1.0.a.2.ip6.arpa crans.ens-cachan.fr v6.crans.org 136.231.138.in-addr.arpa 143.231.138.in-addr.arpa 150.231.138.in-addr.arpa 42.42.10.in-addr.arpa crans.org wifi.crans.org 137.231.138.in-addr.arpa 144.231.138.in-addr.arpa 151.231.138.in-addr.arpa 7.f.0.0.d.3.e.f.0.4.2.0.1.0.a.2.ip6.arpa d.3.e.f.0.4.2.0.1.0.a.2.ip6.arpa wifi.v6.crans.org 138.231.138.in-addr.arpa 145.231.138.in-addr.arpa 186.42.10.in-addr.arpa 9.2.10.in-addr.arpa ferme.crans.org 139.231.138.in-addr.arpa 146.231.138.in-addr.arpa 239.in-addr.arpa adm.crans.org ferme.v6.crans.org 
do
   bash /usr/scripts/gestion/dns/generer_ZSK.sh $zone
done
