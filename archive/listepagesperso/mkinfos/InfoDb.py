#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

"""

  Structure de la base de donn�es : 

     (clef = nom login) ---> (objet de classe CCompte)

 les objets CCompte sont decrits dans CompteRec.py
"""

import shelve

class CInfoDb:
    """ Encapsule une base de donnees de .info

    La structure de cet objet est simple, pour chaque login est stocke de 
    facon persistante l'objet CCompte associe.
    """

    db = None

    def __init__(self,filename,mode="rw"):
        self.db = shelve.open(filename,mode)
        
    def __del__(self):
        self.db.close()

    def Put(self,name,obj):
        """ Ajoute un element."""
        self.db[name] = obj

    def Get(self,name):
        """ recupere un element. """
        return self.db[name]
