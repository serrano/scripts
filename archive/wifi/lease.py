#! /usr/bin/env python
# -*- encoding: iso-8859-15 -*-

# Gestion en lecture d'un fichier lease (issu du DHCPD d'Open)

import re
import time

class Lease:
    """Classe repr�sentant un bail (lease)."""

    def __init__(self, texte):
        """Instanciation d'un bail

        L'instanciation se fait en fournissant `texte' qui est la
        forme textuelle du bail qui se trouve dans
        /var/db/dhcpd.leases de la forme :
        
        lease 10.231.149.89 {
          starts 3 2005/11/09 07:58:52;
          ends 3 2005/11/09 08:13:52;
          hardware ethernet 00:0c:f1:37:54:2c;
          uid 01:00:0c:f1:37:54:2c;
          client-hostname "FCDVIRELY";
        }
        """
        mo = re.search("\\blease ([0-9\\.]*) \\{", texte)
        if not mo:
            raise ValueError, u"Pas de bail trouv�"
        self.ip = mo.group(1)

        mo = re.search("\\bstarts [0-7] ([0-9/]* [0-9:]*);", texte)
        if not mo:
            raise ValueError, u"Pas de date de d�but de bail"
        self.start = time.strptime(mo.group(1), "%Y/%m/%d %H:%M:%S")

        mo = re.search("\\bends [0-7] ([0-9/]* [0-9:]*);", texte)
        if not mo:
            raise ValueError, u"Pas de date de fin de bail"
        self.end = time.strptime(mo.group(1), "%Y/%m/%d %H:%M:%S")

        mo = re.search("\\bhardware ethernet ([0-9a-f:]*);", texte)
        if not mo:
            raise ValueError, u"Pas d'adresse Ethernet pour le bail %s" % self.ip
        self.mac = mo.group(1)

        # On n'est pas int�ress� par le reste

class Leases:
    """Classe repr�sentant un ensemble de bails."""

    def __init__(self, fichier="/var/db/dhcpd.leases"):
        """Instanciation � partir du fichier de leases."""
        self.leases = []
        # M�thode rapide :
        for lease in " ".join(file(fichier).readlines()).split("}")[:-1]:
            try:
                nouveau = Lease(lease)
                # On enleve les anciens
                self.leases = filter(lambda x: x.ip != nouveau.ip, self.leases)
                # On rajoute le nouveau
                self.leases.append(nouveau)
            except ValueError:
                # Certains baux peuvent ne pas avoir d'adresse MAC
                pass
