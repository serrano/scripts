#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

# ce script ajoute une ligne � la fin du fichier des utilisateurs
# avec le mot de passe d�ja hash�, il faut ensuite l'integrer au 
# dictionnaire

import sha, sys
from getpass import getpass

sys.stdout.write('Nom d\'utilisateur (au format prenom.nom) : ')
while True :
    user = sys.stdin.readline().strip()
    if user :
        break

while True :
    pass1 = getpass('Mot de passe : ')
    pass2 = getpass('Retappez le mot de passe : ')
    if pass1 == pass2 :
        break
    print 'Les deux mot de passe ne correpondent pas !'

f = open('/usr/scripts/wifiweb/utilisateurs.py','a+')
f.write('# %s:%s\n' % (user , sha.new(pass1).hexdigest()) )
f.close()
