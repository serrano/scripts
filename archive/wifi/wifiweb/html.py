#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

class html :
    
    def __init__ (self) :
        self._titre = ''
        self._sous_titre = ''
        self._corp = ''
        self._refresh = 0
    
    def titre(self, titre = None) :
        """ D�finit ou retourne le titre """
        if titre != None :
            self._titre = titre
        return self._titre
    
    def sous_titre (self, sous_titre = None) :
        """ D�finit ou retourne le sous titre """
        if sous_titre != None :
            self._sous_titre = sous_titre
        return self._sous_titre
    
    def refresh (self, refresh = None) :
        """ D�finit ou retourne la dur�e du refresh """
    
        if _refresh != None :
            self._refresh = _refresh
        return self._refresh
    
    def corp (self, corp = None) :
        """ D�finit ou retourne le contenu du corp """
        if corp != None :
            self._corp = corp
        return self._corp
    
    def add (self, string) :
        """ Ajoute une ligne au corp de la page """
        self._corp += string + "\n"
    
    def make (self) :
        """ G�n�re la page HTML finiale """
    
        page = ""
    
        page += "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
        page += "<html lang=\"fr\">\n"
    
        # en-t�tes de la page
        #####################
    
        page += "<head>\n"
        page += "  <title>%s</title>\n" % self._titre
        if self._refresh :
            page += "  <meta http-equiv=\"refresh\" content=\"%s\">\n" % str(self._refresh)
        page += "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n"
        page += "</head>\n\n"
    
        # d�but du corp de la page
        ##########################
    
        page += "<body background=\"http://www.crans.org/styles/style2_left.png\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"background-repeat:repeat-y\">\n"
        page += "<img src=\"http://www.crans.org/styles/style2_top.png\" alt=\"En_tete\"><br>\n\n"

        # division du titre
        page += "<div id=\"Titre\" style=\"position:absolute; left:580px; top:70px; width:400px;height:34px; z-index:2\">\n"
        page += "<font size=\"5\"><b>%s</b></font>\n" % self._titre
        page += "</div>\n\n"
        
        # division du sous titre
        page += "<div id=\"Sous-titre\" style=\"position:absolute; left:380px; top:140px; width:500px;height:34px; z-index:2\">\n"
        page += "<font size=\"4\"><u>%s</u></font>\n" % self._sous_titre
        page += "</div>\n\n"
    
        # division du contenu
        page += "<div id=\"Contenu\" style=\"position:absolute; left:245px; top:190px; right:16px; z-index:1; overflow: visible; visibility: visible; background-color: #FFFFFF; layer-background-color: #FFFFFF;\">\n"
        page += self._corp
        page += "</div>\n\n"
    
        # fin de la page
    ################
    
        page += "</body>\n"
        page += "</html>\n"
   
        return page
    
    def sendheaderstobrowser (self) :
        """ Envoie les entetes au navigateur """
        print "content-type: text/html"
        print

    def sendtobrowser (self) :
        """ Envoie la page au navigateur """
        print self.make()
    
    def savetofile (self, fichier) :
        """ Enregistre la page dans un fichier """
        f = open(fichier,'w')
        f.write( self.make() )
        f.close()
