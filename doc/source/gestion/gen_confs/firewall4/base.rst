
base -- Le pare-feu ipv4 de base
================================

.. automodule:: gestion.gen_confs.firewall4.base
   :members:
   :special-members:
