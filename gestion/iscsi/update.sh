#!/bin/bash

# update.sh
# ---------
# Modifié : Pierre-Elliott Bécue <becue@crans.org> (8 août 2012)
# Copyright : (c) 2008, Jeremie Dimino <jeremie@dimino.org>
# Licence   : BSD3

# Ce script fait tout ce qu'il faut après l'ajout d'un volume sur la
# baie de stockage.

exec_cmd() {
    local msg=$1
    shift
    local cmd="$@"
    if tty -s <&1; then
        echo -e "\e[37;1m===== $msg\e[0m"
        echo -e "\e[1m=> commande: $cmd\e[0m"
    else
        echo "===== $msg"
        echo "=> commande: $cmd"
    fi
    if [[ $UID = 0 ]]; then
        $cmd
    else
        sudo $cmd
    fi
}

exec_cmd "Récupération du mapping lun<->nom de volume" \
    /usr/scripts/gestion/iscsi/get_volume_mapping.py nols
    /usr/scripts/gestion/iscsi/get_volume_mapping.py slon

exec_cmd "Rechargement des règles de udev" \
    invoke-rc.d udev reload

exec_cmd "Rescan des volumes iscsi" \
    iscsiadm -m session --rescan

exec_cmd "Mises à jours des liens symboliques dans /dev" \
    /usr/scripts/gestion/iscsi/udev_update_symlinks.py --clean
