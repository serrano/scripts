#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import socket
sys.path.append('/usr/scripts')
sys.path.append('/etc/crans')

import services

if not 'crans-nfs' in services.services or not socket.gethostname() in services.services['crans-nfs']:
    sys.stderr.write("Devrait être exécuté sur une machine du groupe 'crans-nfs'\n")
    exit(1)
if len(sys.argv) < 1:
    sys.stderr.write("Usage : %s [user1] [user2] [user3] ...\n" % sys.argv[0])
    exit(1)

import lc_ldap.shortcuts
import gestion.gen_confs.adherents

conn=lc_ldap.shortcuts.lc_ldap_readonly()

l=[]
for user in sys.argv[1:]:
    a=conn.search(u'uid=%s' % user)
    if a and a[0]['homeDirectory'] and a[0]['uidNumber'] and a[0]['uid']: 
        l.append("%s,%s,%s" % (a[0]['homeDirectory'][0], a[0]['uidNumber'][0], a[0]['uid'][0]))

if l:
    h=gestion.gen_confs.adherents.home(l)
    h.reconfigure()
