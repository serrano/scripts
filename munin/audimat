#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Système de monitoring des abonnés au différents flux multicast

Frédéric Pauget, Nicolas Salles, Etienne Chové
"""

import sys, os
sys.path.append('/usr/scripts/gestion')
from hptools import hpswitch
from annuaires_pg import all_switchs, uplink_prises

####################################################################
## Définition des chaines à audimater
####################################################################

# On construit un dictionnaire au format "ip" : "nom_de_la_chaine"


# On log les chaines qui font des annonces sap
#   on essaie de télécharger le fichier sap.txt
#   si on y arrive pas on continue avec l'ancien

chaines = {}

try :
    os.system("/usr/bin/wget http://television.crans.org/sap.txt -O /tmp/sap.txt -q 2> /dev/null")
except :
    pass

# lecture du fichier
diffusion = open ('/tmp/sap.txt','r')
for l in diffusion.readlines() :
    chaines[l.rstrip().split(":")[1]] = l.split(":")[0].replace('.','_').replace('-','_')[0:18]

####################################################################
## Configuration pour munin
####################################################################

if 'autoconf' in sys.argv :
    print "yes" # Surement y rajouter le up du backbone, ...
    sys.exit(0)

if 'config' in sys.argv :
    print "host_name backbone"
    print "graph_title AudimatTV";
    print "graph_args --base 1000";
    print "graph_vlabel nb clients/chaines";
    inc = 0
    chaines = chaines.values()
    chaines.sort()
    for nom in chaines :
        if not nom : continue
        nom1 = nom.replace(' ','_').replace("'", '')
        print "%s.label %s" % (nom1, nom)
        if inc == 0 :
            print "%s.draw AREA" % nom1
        else:
            print "%s.draw STACK" % nom1
        inc = 1
    print "total.label Total"
    sys.exit(0)

####################################################################
## Mesure de l'audimat, lancé par cron à cause du timeout de munin
####################################################################

if 'generate' in sys.argv :
    stats = {} # { IP :  [ prises ] }

    for switch in all_switchs() :
        # on vérifie que le switch est pingable
        if os.system('ping -c 3 %s > /dev/null 2> /dev/null' % switch ) :
            continue

        sw = hpswitch(switch)
        switch=switch.split('.')[0]
        if switch[-1].isalpha() :
            bat = switch[-1].lower()
            sw_num = 0
        else :
            bat = switch[-3].lower()
            sw_num = int(switch[-1])

        #if bat not in uplink_prises:
        #    continue
        for ip, liste in sw.multicast().items() :
            stats.setdefault(ip,[])
            for port in liste :
                num_port = 100 * sw_num + int(port)
                if num_port not in uplink_prises[bat].keys() :
                    stats[ip].append('%s%03i' % (bat, num_port) )

    total = 0

    f = open('/var/lib/munin/tmp/audimat','w')

    for ip, nom in chaines.items() :
        if not nom : continue
        nb_clients = len(stats.get(ip,[]))
        total += nb_clients
        if 'prises' in sys.argv :
            f.write( "%s (%i) : %s\n" % (nom, nb_clients, ', '.join(stats.get(ip,[])).upper() ) )
        else :
            f.write( "%s.value %i\n" % ( nom.replace(' ','_'), nb_clients ) )

    f.write("total.value %i" % total )
    sys.exit(0)

####################################################################
## Retour des audimats pour munin
####################################################################

try :
    print file("/var/lib/munin/tmp/audimat").read()
except :
    pass
