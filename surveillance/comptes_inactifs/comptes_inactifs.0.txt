Encoding: utf-8
Subject: [CRANS] Compte inactif depuis plus d'un mois

Bonjour %(nom)s,

Si tu t'es connecté(e) (par ssh, pop, imap ou webmail) dans les trois
dernières heures, tu peux ignorer ce message.

Tu reçois ce mail parce que tu as un compte sur le serveur des adhérents du
CRANS, et que tu ne t'es pas connecté(e) depuis le %(date)s (ou tu
ne t'es jamais connecté(e)), ce qui fait plus d'un mois.

Malgré la redirection via le fichier .forward dans ton home, tu as %(mail)d
nouveau(x) mail(s) dans ta boîte de réception. Ceux-ci peuvent être là depuis
avant la mise en place de la redirection, ou parce qu'ils n'ont pas été
filtrés par procmail, par exemple.

Nous t'invitons à te connecter (via le webmail http://webmail.crans.org par
exemple) afin de lire ces mails.

Nous te prions de ne pas répondre à ce mail, sauf pour nous signaler une
anomalie ou confirmer un abandon définitif du compte.

-- 
Script de détection des comptes inactifs
