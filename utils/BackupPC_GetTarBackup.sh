#!/bin/sh

# La syntaxe de BackupPC_tarCreate est un peu chiante... Ce wrapper facilite le travail pour extraire une sauvegarde
# NS. 18 juin 2005

host=$1
shareName=$2
Destdir=$3
AddPath=$4
args=$#

CheckHost() {
    test -f /var/lib/backuppc/pc/$host/backups || { echo "No backups for host $host !" ; exit 1 ; }
    LastBackup=$(tail -1 /var/lib/backuppc/pc/$host/backups | cut -f 1)
}

CheckDestDir() {
    if [ "$Destdir" != "" ] ; then
	test -d $Destdir || { echo "Destination directory not found !" ; exit 1 ; }
	Destdir="$Destdir/"
    fi
}

Usage() {
    if [ $args -lt 2 ] ; then
	echo "Usage: $0 host shareName [Destdir] [AddPath]" >&2
	exit 1
    fi
}

Usage
CheckHost
CheckDestDir
#echo Last backup for $1 is number $LastBackup

/bin/su backuppc -c "/usr/share/backuppc/bin/BackupPC_tarCreate -h $host -n $LastBackup -s $shareName -r / -p $AddPath/ . > ${Destdir}${host}.${shareName}.tar"

