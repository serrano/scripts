#!/bin/sh

# Pour que www-data puisse créer les fichiers à imprimer

getent passwd $1 2>&1 >/dev/null
exists=$?
if [ $exists -eq 0 ]; then
	mkdir -p /home/impressions/$1
	chown -R $1:www-data /home/impressions/$1
	chmod -R 770 /home/impressions/$1
else
	echo "Le dossier /home/$1 n'existe pas." >&2
fi
