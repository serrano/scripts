# -*- coding: iso-8859-1 -*-

import os
import PagesPerso

class ClubList(PagesPerso.AccountList):
    home = "/home/club"
    www = ""
    url = "http://clubs.ens-cachan.fr/%s/"

    def comptes(self):
        """Retourne la liste des comptes"""
        return filter(lambda x: os.path.isdir(u"/home/c/club/%s" % x) and  not os.path.islink(u"/home/c/club/%s" % x),
                      os.listdir(u"/home/c/club"))

def execute(macro, args):
    return macro.formatter.rawHTML(ClubList().to_html())

