# -*- encoding: utf-8 -*-

import os

class AccountList:
    home = "/home"
    www = "www"
    url = "http://perso.crans.org/%s/"

    def __init__(self):
        return

    def comptes(self):
        """Retourne la liste des comptes"""
        return filter(lambda x: os.path.isdir(u"/home/%s/www" % x) and  not os.path.islink(u"/home/%s/www" % x),
        #return filter(lambda x: os.path.isdir(os.path.expanduser(u"~%s/www" % x)) and  not os.path.islink(os.path.expanduser(u"~%s/www" % x)),
                      os.listdir(u"/home/mail"))
        ### ^^^^^^ le code m'a tuer


    def makeAnchor(self,letter):
        return u"<div class=\"vignetteperso\"><a class=\"letter_anchor\" name=\"index_%s\"><span>%s:</span></a></div>" % ( letter, letter )

    def makeIndex(self,letter_list):
        index = u''
        for aLetter in letter_list:
            index = u"%s<a href=\"#index_%s\">%s</a>" % ( index, aLetter, aLetter)
        return u"<div class=\"alphabetic_index\">%s</div>" % index

    def to_html(self):
        dirs = self.comptes()
        dirs.sort()
        html = u""

        premiere_lettre = ''
        letter_list = []
        for d in dirs:
            if premiere_lettre != d[0]:
                premiere_lettre = d[0]
                letter_list.append(premiere_lettre)
                html = u"%s\n%s" % ( html, self.makeAnchor(premiere_lettre) )
            html = u"%s\n%s" % (html, Account(self.home, d, self.www, self.url).to_html())
            
        index = self.makeIndex(letter_list)
        html = index + html
        html += u'<br style="clear: both">'
        return html

class Account:
    """Classe représentant la page perso d'une personne"""

    def __init__(self, home, login, www, url):
        """Instanciation avec le `login' de la personne"""
        self.login = login
        self.home = "%s/%s" % (home, login)
        #self.home = #os.path.expanduser("~%s" % login)
        self.www = www
        self.url = url

    _info = None
    def info(self, champ):
        """Retourne le contenu du champ `champ' dans le fichier info"""
        if self._info == None:
            try:
                lignes = file("%s/.info" % self.home)
            except IOError:
                lignes = []

            # self._info est un dictionnaire qui reprend le contenu du .info
            self._info = dict(map(lambda z: (unicode(z[0].lower(),"iso-8859-15"),
                                             unicode(z[1],"iso-8859-15")),
                                  filter(lambda w: len(w) == 2 and len(w[1]),
                                         map(lambda x: map(lambda y: y.strip(),
                                                           x.split(":")),
                                             lignes))))

        if self._info.has_key(champ.lower()):
            return self._info[champ.lower()]
        else:
            return u""

    def chemin(self):
        """Chemin vers le www"""
        return os.path.join(self.home, self.www)

    def logo(self):
        """URL du logo s'il y en a un"""
        if self.info("logo"):
            # Le logo peut être en absolu ou en relatif
            if self.info("logo").startswith(self.chemin()):
                logo = self.info("logo").replace("%s/" % self.chemin(), "")
            else:
                logo = self.info("logo")
            if os.path.isfile(os.path.join(self.chemin(), logo)):
                return u"%s%s" % (self.url % self.login, logo)
        return u"http://perso.crans.org/pageperso.png"

    def to_html(self):
        """Renvoie le code HTML correspondant au fichier .info"""
        html = [ u'<div class="vignetteperso">',
                 u'<a href="%s">' % (self.url % self.login),
                 u'<img src="%s" alt="%s">' % (self.logo(), self.login),
                 u'</a><br>',
                 self.info("nom") and u'<b>%s</b><br>' % self.info("nom") or u'%s<br>' % self.login,
                 self.info("devise") and u'<i>%s</i>' % self.info("devise") or u'',
                 u'</div>' ]
        return u'\n'.join(html)


def execute(macro, args):
    return macro.formatter.rawHTML(AccountList().to_html())
