# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - YouTube Macro
    Jesus L. Alvaro 2006
    v 0.0.2
    You can include YouTube videos in the wiki by using this macro:
        [[YouTube(V8tSRJ8e3b0)]]  or
        [[YouTube(http://www.youtube.com/v/V8tSRJ8e3b0)]]
    visit "http://www.iesvaldebernardo.es/w/Post/2006-11-30-1447/YouTube_en_la_Wiki."

"""

def execute(macro, text):
    if text.find('http://')> -1:
        try:
            text = text.split('v=')[1]
        except:
            return u"URL non valide..."
    url = 'http://www.youtube.com/v/%s' % text
    html = u'''
    <object width="425" height="350">
    <param name="movie" value="%(youtubelink)s"></param>
    <param name="wmode" value="transparent"></param>
    <embed src="%(youtubelink)s" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed>
    </object>
    ''' % {"youtubelink": url}
    return macro.formatter.rawHTML(html)
